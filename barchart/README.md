# ASCII Bar Chart

Let's assume we have an array of data. The array consists of objects with two parameters: a label (a string) and a value (number). For example
```javascript
const data = [
    { label: 'January', value: 6 },
    { label: 'February', value: 3 },
    { label: 'March', value: 4 },
    { label: 'April', value: 9 },
    { label: 'May', value: 2 },
    { label: 'June', value: 1 },
    { label: 'July', value: 12 },
    { label: 'August', value: 11 },
    { label: 'September', value: 10 },
    { label: 'October', value: 6 },
    { label: 'November', value: 0 },
    { label: 'December', value: 2 }
]
```

Create a program that given such data array, draws an ASCII bar graph of the data. The bar chart should have all the labels on the x-axis, and over each label should be a column made of #-symbols with its height corresponding to the value of the data object. The y-axis should be numbered.

With the above data the output should look something like this
![](barchart1.png)

## Extra: Negative values

Update your bar chart generator so that it accepts negative numbers and shows them below the x-axis.
![](barchart2.png)