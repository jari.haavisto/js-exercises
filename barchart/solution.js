function barChart(data) {
    const maxValue = data.reduce((acc, cur) => Math.max(cur.value, acc), -Infinity)
    const minValue = data.reduce((acc, cur) => Math.min(cur.value, acc), +Infinity)

    const marginWidth = Math.max(maxValue, -minValue).toString().length + 4
    const chartWidth = data.reduce((acc, cur) => acc + cur.label.length + 2, 0) + marginWidth - 2
    const barSymbol = '#'

    let chart = ''

    for (let i = maxValue; i > 0; i--) {
        const margin = i + ' | '
        chart = chart + ' '.repeat(marginWidth - margin.length) + margin
        for (let j = 0; j < data.length; j++) {
            const char = data[j].value >= i ? barSymbol : ' '
            chart = chart + char + ' '.repeat(data[j].label.length + 1)
        }
        chart = chart + '\n'
    }

    chart = chart + '-'.repeat(chartWidth) + '\n'
    chart = chart + ' '.repeat(marginWidth)
    for (let i = 0; i < data.length; i++) {
        chart = chart + data[i].label + '  '
    }
    chart = chart + '\n' + '-'.repeat(chartWidth) + '\n'

    for (let i = -1; i >= minValue; i--) {
        const margin = i + ' | '
        chart = chart + ' '.repeat(marginWidth - margin.length) + margin
        for (let j = 0; j < data.length; j++) {
            const char = data[j].value <= i ? barSymbol : ' '
            chart = chart + char + ' '.repeat(data[j].label.length + 1)
        }
        chart = chart + '\n'
    }

    console.log(chart)
}

const data = [
    { label: 'January', value: -8 },
    { label: 'February', value: -3 },
    { label: 'March', value: -1 },
    { label: 'April', value: 0 },
    { label: 'May', value: 2 },
    { label: 'June', value: 6 },
    { label: 'July', value: 12 },
    { label: 'August', value: 11 },
    { label: 'September', value: 10 },
    { label: 'October', value: 6 },
    { label: 'November', value: 0 },
    { label: 'December', value: -2 }
]

barChart(data)

