# Shuffle and Sort

Arranging array items in order is a very often needed task. All modern programming languages have native functions for sorting, and using them is practically always better choice than implementing your own. This does not mean that creating your own functions would not be interesting or fun.

In this exercise you will create a shuffle function and two sorting functions. You are not allowed to use any premade sorting capabilities, namely the .sort function. You *are* allowed to use the Math.random function, since creating your own random function is very difficult.

The functions you create should work "in place". This means that you should not create a new array at any point, but that the sorting should happen by switching around elements of the parameter array. You will need at least one temporary variable to hold values while swapping.

## Shuffle

Shuffle arranges the elements of an array in random order. The shuffle function should work in one pass, as in it should not need to iterate through the array more than one time.

Shuffle is a so called **O(n)** algorithm, since for an array with n elements it requires n steps to randomize it.

## Bubble Sort

Bubble sort is one of the most simple sort algorithms. The idea behind bubble sort is that it does repeated passes through the array, moving the largest number it finds forward as it progresses. This means that the largest numbers "bubble" to the top like bubbles in soda.

Bubble sort starts by comparing first and second elements. If the first element is larger, it swaps them. If the first element is smaller, the pair is already in order, so the algorithm does nothing. Then it compares second and third elements. Again, if the element with lower index is larger, the elements are swapped. This is repeated until the end of the array. Now we know that the largest element of the array is at the end.

Then it repeats this process. On the second pass it can skip the last comparison, since it knows the last element is already in order. On third pass it can skip two of the last comparisons etc. It continues this either until every item has been moved, or it finishes a full pass without any swaps.

Bubble sort is a so called **O(n<sup>2</sup>)** algorithm. This means that if the array being sorted has n elements, the algorithm needs n<sup>2</sup> steps to finish in the worst case. This is evident since bubble sorts needs n steps for every pass it makes (even if the passes get shorter we still count them as full passes), since it needs to go through every item of the array, and it is possible that the elements are arranged so that it needs n passes to finish the sorting.

When implementing bubble sort, you should first create a bubble sort function that sorts the algorithm in n<sup>2</sup> passes and ignore the optimizations. Meaning that you should not shorten the passes on each step and you should not check if the algorithm should finish early because it has done a full pass without any swaps. When you have a working basic version and you understand how it works, you can optimize it further.

![](bubble_sort.gif)

## Bonus: Cocktail Shaker Sort

This sort is for those who are already familiar with the bubble sort and want a bit more challenge. Cocktail shaker sort is a "bi-directional bubble sort". It starts just like bubble sort. The first pass finds the largest item and bubbles it to the end of the array.

Then the algorithm repeats *backwards*, starting from the last item, again comparing each pair and swapping if necessary, until it gets back to the beginning of the array. Now we know that the *smallest* element is at the beginning of the array. 

Then the algorithm repeats until all the items are sorted.

Let's look at an example case. We have an array of 9 items: [ 7, 4, 2, 9, 3, 8, 6, 1, 5].

First pass:
1. Numbers at positions 0 (number 7) and 1 (number 4) are compared. 
    - Since the earlier number (7) is larger than the later number (4), the elements are swapped. 
    - The resulting array is [ 4, 7, 2, 9, 3, 8, 6, 1, 5]
2. Numbers at positions 1 (number 7) and 2 (number 2) are compared.
    - Since the earlier number (7) is larger than the later number (2), the elements are swapped.
    - The resulting array is [ 4, 2, 7, 9, 3, 8, 6, 1, 5]
3. Numbers at positions 2 (number 7) and 3 (number 9) are compared.
    - Since the earlier number (7) is smaller, no swap happens
4. Numbers at positions 3 (number 9) and 4 (number 3) are compared.
    - Again, the first number is larger and a swap happens
    - The resulting array is [ 4, 2, 7, 3, 9, 8, 6, 1, 5]
5. Number 9 being the largest keeps on being swapped, moving up on every step until the end of the first pass.
    - The resulting array after the first pass is [ 4, 2, 7, 3, 8, 6, 1, 5, 9]

Second pass: 
1. Numbers at positions 7 (number 5) and 6 (number 1) are compared. We do not need to include the last number in the comparisons since we know it is the largest and hence at the correct place.
    - Since the numbers are in order, they are not swapped.
    - The resulting array is [ 4, 2, 7, 3, 8, 6, 1, 5, 9]
2. Numbers at positions 6 (number 1) and 5 (number 6) are compared.
    - Since the numbers are not in order, they are swapped.
    - The resulting array is [ 4, 2, 7, 3, 8, 1, 6, 5, 9]
3. Number 1 being the smallest keeps on being swapped, moving down on every step until the end of the second pass.
    - The resulting array after the second pass is [ 1, 4, 2, 7, 3, 8, 6, 5, 9]

The third pass would start from the index 1, since we know that the index 0 contains the smallest number. The third pass would continue to the end of the array, excluding the last element, and so forth.

![](cocktail_shaker_sort.gif)