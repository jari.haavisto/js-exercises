
function cocktailShakerSort(arr) {
    let temp, swapHappened
    let startIndex = 0
    let endIndex = arr.length - 2
    while (true) {
        swapHappened = false
        for (let i = startIndex; i <= endIndex; i++) {
            if (arr[i] > arr[i + 1]) {
                temp = arr[i]
                arr[i] = arr[i + 1]
                arr[i + 1] = temp
                swapHappened = true
            }
        }
        if (!swapHappened) {
            break
        }
        endIndex = endIndex - 1

        swapHappened = false
        for (let i = endIndex; i >= startIndex; i--) {
            if (arr[i] > arr[i + 1]) {
                temp = arr[i]
                arr[i] = arr[i + 1]
                arr[i + 1] = temp
                swapHappened = true
            }
        }
        if (!swapHappened) {
            break
        }
        startIndex = startIndex + 1
    }
}

const arr = [7, 3, 8, 12, 5, 1, 14, 39]
cocktailShakerSort(arr)
console.log(arr)