function shuffle(arr) {
    let temp
    for (let i = 0; i < arr.length - 1; i++) {
        const randomIndex = Math.floor(Math.random() * (arr.length - i)) + i
        temp = arr[i]
        arr[i] = arr[randomIndex]
        arr[randomIndex] = temp
    }
    return arr
}

const numbers = [1,2,3,4,5,6,7,8,9]
console.log(shuffle(numbers))



// The simplest version
function bubbleSort1(arr) {
    let temp
    for (let i = 0; i < arr.length - 1; i++) {
        for (let j = 0; j < arr.length - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                temp = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = temp
            }
        }
    }
}

// Slightly optimized version
function bubbleSort2(arr) {
    let temp, swapHappened

    for (let i = 0; i < arr.length - 1; i++) {
        swapHappened = false
        for (let j = 0; j < arr.length - 1 - i; j++) {
            if (arr[j] > arr[j + 1]) {
                temp = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = temp
                swapHappened = true
            }
        }
        if (!swapHappened) {
            console.log(`Saved ${arr.length - 1 - i} rounds!`)
            break
        }
    }
    return arr
}

const arr = [7, 3, 8, 12, 5, 1, 14, 39]
console.log(bubbleSort2(arr))


function cocktailShakerSort(arr) {
    let temp, swapHappened
    let startIndex = 0
    let endIndex = arr.length - 2
    while (true) {
        swapHappened = false
        for (let i = startIndex; i <= endIndex; i++) {
            if (arr[i] > arr[i + 1]) {
                temp = arr[i]
                arr[i] = arr[i + 1]
                arr[i + 1] = temp
                swapHappened = true
            }
        }
        if (!swapHappened) {
            break
        }
        endIndex = endIndex - 1

        swapHappened = false
        for (let i = endIndex; i >= startIndex; i--) {
            if (arr[i] > arr[i + 1]) {
                temp = arr[i]
                arr[i] = arr[i + 1]
                arr[i + 1] = temp
                swapHappened = true
            }
        }
        if (!swapHappened) {
            break
        }
        startIndex = startIndex + 1
    }
    return arr
}

console.log(cocktailShakerSort(arr))