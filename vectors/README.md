# Vector Calculations

In linear algebra, a vector can be thought as an ordered set of elements, usually numbers. In programming we can represent a vector with an array. For example, a three-dimensional vector 2**i** + 3**j** + 5**k** could be represented as `[2,3,5]`. Of course we are not limited to three dimensions.


## Vector sum of two vectors

If we have two vectors, `a = [a1, a2, a3]` and `b = [b1, b2, b3]`, then their vector sum is defined as `a + b = [a1+b1, a2+b2, a3+b3]`. The vector sum can only be calculated to vectors with equal dimensions.

Create a function that takes in two vectors, represented as arrays, and calculates their sum. If the vectors can not be added together, the function prints an error message and returns undefined. The function must be able to calculate the sum of vectors of any dimension, as long as both are of the same dimension.


## Vector sum

Create another function that takes any number of vectors as parameters and calculates their sum like.


## Dot product

The dot product is the sum of the products of the corresponding entries of the two sequences of numbers. If we have two vectors, `a = [a1, a2, a3]` and `b = [b1, b2, b3]`, then their dot product is defined as `a·b = a1*b1 + a2*b2 + a3*b3`. The result of the dot product is a number, not a vector. The dot product can only be calculated to vectors with equal dimensions.

Create a function that takes two vectors, represented as arrays, and calculates their dot product. Mathematically it does not make sense to calculate the dot product of more than two vectors.

## Vector calculator

Create a vector calculator function that takes the name of the operation as its first parameter. This parameter is a string. Then it takes any number of vectors as parameters and performs the desired calculation on them.

Of course, if the inputs are invalid in any way, the calculator prints an error message and returns undefined.


## Outer Product (difficult)

The result of an outer product operation is not a number or a vector, it is a matrix. If the two vectors have dimensions n and m, then their outer product is an n × m matrix. 

Matrix can be thought of as a row of vectors, so it can be represented in a code as an array of arrays. For example, a matrix
![](matrix.svg)
could be represented as
```
A = [
    [-1.3, 0.6],
    [20.4, 5.5],
    [9.7, -6.2]
]
```
Now we can access the matrix using two dimensional coordinates. For example, `A[0][1] = 0.6` where the first coordinate indicates which row we're looking at, and the second the column.

The outer product is defined so that if we have two vectors `a = [a1, a2, a3, a4]` and `b = [b1, b2, b3]`, the resulting matrix is
```
a × b = [
    [a1*b1, a1*b2, a1*b3],
    [a2*b1, a2*b2, a2*b3],
    [a3*b1, a3*b2, a3*b3],
    [a4*b1, a4*b2, a4*b3],
]
```
As in, the height of the resulting matrix corresponds to the length of the first vector, and the width of the matrix corresponds to the length of the second vector. Notice that in this case the vectors do *not* need to be of equal length.

Create a function that takes two vectors, represented as arrays, and calculates their outer product. Add the outer product to the calculator function you previously created.