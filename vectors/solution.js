function isValidVector(vector) {
    return vector.every(element => typeof element === 'number')
}

function areValidVectors(...vectors) {
    return vectors.every(isValidVector)
}

function vectorSumOfTwoVectors(vector1, vector2) {
    if (!areValidVectors(vector1, vector2)) {
        return console.log('Invalid inputs')
    }

    if (vector1.length !== vector2.length) {
        return console.log('Invalid vector dimensions')
    }

    const sum = []
    for (let i = 0; i < vector1.length; i++) {
        sum.push(vector1[i] + vector2[i])
    }
    return sum
}


const a = [1, 2, 3, 4]
const b = [2, 3, 4, 5]

console.log('Vector sum of two vectors', vectorSumOfTwoVectors(a, b))

function vectorSum(...vectors) {
    if (!areValidVectors(...vectors)) {
        return console.log('Invalid inputs')
    }

    if (vectors.some(vector => vector.length !== vectors[0].length)) {
        return console.log('Invalid vector dimensions')
    }

    const sum = []
    for (let i = 0; i < vectors[0].length; i++) {
        let iSum = 0
        for (let j = 0; j < vectors.length; j++) {
            iSum += vectors[j][i]
        }
        sum[i] = iSum
    }
    return sum
}

const c = [3, 4, 5, 6]

console.log('Vector sum', vectorSum(a, b, c))

function dotProduct(vector1, vector2) {
    if (!areValidVectors(vector1, vector2)) {
        return console.log('Invalid inputs')
    }

    if (vector1.length !== vector2.length) {
        return console.log('Invalid vector dimensions')
    }

    let product = 0
    for (let i = 0; i < vector1.length; i++) {
        product += vector1[i] * vector2[i]
    }
    return product
}

console.log('Dot product', dotProduct(a, b))

function calculator(operator, ...vectors) {
    if (vectors.length < 2) {
        return console.log('Can not perform calculations on less than two vectors')
    }

    if (operator === '+') {
        return vectorSum(...vectors)
    }
    if (operator === '*') {
        if (vectors.length > 2) {
            return console.log('Cannot perform product on more than two vectors')
        }

        return dotProduct(...vectors)
    }

    if (operator === 'x') {
        if (vectors.length > 2) {
            return console.log('Cannot perform product on more than two vectors')
        }
        return outerProduct(...vectors)
    }

    console.log('Invalid operator')
}

console.log('Calculator')
console.log(calculator('+', a, b, c))
console.log(calculator('*', a, c))

function outerProduct(vector1, vector2) {
    const product = []

    for (let i = 0; i < vector1.length; i++) {
        product[i] = []
        for (let j = 0; j < vector2.length; j++) {
            product[i][j] = vector1[i] * vector2[j]
        }
    }

    return product
}

console.log(calculator('x', a, b))